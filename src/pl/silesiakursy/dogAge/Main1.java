package pl.silesiakursy.dogAge;

public class Main1 {
     static double dogAge(double age) {
         /* dla pierwszych dwóch lat, każdy rok życia psa jest równy 10.5 ludzkiego roku,
          powyżej dwóch lat, każdy rok życia psa, to 4 ludzkie lata,*/
          double dogAgeToHumenAge = 0.0;
          if (age > 0 && age <= 2) {
               dogAgeToHumenAge = age * 10.5;
          } else if (age > 0 && age > 2) {
               dogAgeToHumenAge = (2 * 10.5) + ((age - 2) * 4.0);
          } else {
               System.out.println("Podano błędny wiek psa");
          }
          return dogAgeToHumenAge;
     }
}