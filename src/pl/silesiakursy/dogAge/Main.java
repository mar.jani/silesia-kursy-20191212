package pl.silesiakursy.dogAge;

import java.util.Scanner;

import static pl.silesiakursy.dogAge.Main1.dogAge;

public class Main {

    public static void main(String[] args) {
        menu();
    }

    public static void menu(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wiek psa: ");
        double age = scanner.nextDouble();
        System.out.println ("Wiek psa w przeliczeniu na wiek człowieka: " + dogAge(age));
    }
}
