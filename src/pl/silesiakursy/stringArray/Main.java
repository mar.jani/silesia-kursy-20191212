package pl.silesiakursy.stringArray;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę:");
        String string = scanner.nextLine();

        if (string.contains(",")) {
            String[] strings = string.split(",");
            Double intValue = Double.valueOf(strings[0]);
            Double floatedValue = Double.valueOf(strings[1]);
            Double result = intValue + floatedValue * 0.1;
            System.out.println("Wynik to: " + result);
        } else if (string.contains(".")) {
            String[] strings = string.split("\\.");
            Double intValue = Double.valueOf(strings[0]);
            Double floatedValue = Double.valueOf(strings[1]);
            Double result = intValue + floatedValue * 0.1;
            System.out.println("Wynik to: " + result);
        }
    }


}
